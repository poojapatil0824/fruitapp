//
//  FruitViewModel.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class FruitViewModel {
    private let fruitWebService: FruitWebService
    
    init(fruitWebService: FruitWebService) {
        self.fruitWebService = fruitWebService
    }
    
    public func getFruitDataFromService(completion: @escaping (_ fruit : [FruitDetails]?, _ error: Error?) -> Void) {
        fruitWebService.fetchFruitDataFromServer() { (fruit, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let fruit = fruit  else { return }
            return completion(fruit, nil)
        }
        
    }
    public func convertPrice(fruitPrice: Int) -> String {
        let price: Double = Double(fruitPrice) / 100
        return ("Price : " + "£" + String(price))
    }
    public func convertWeight(fruitWeight: Int) -> String {
        let weight: Double =  Double(fruitWeight) / 1000
        return ("Weight : " + String(weight) + "Kg")
    }
    
}
