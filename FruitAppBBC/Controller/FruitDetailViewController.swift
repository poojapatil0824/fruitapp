//
//  FruitDetailViewController.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class FruitDetailViewController: UIViewController {
    @IBOutlet weak var fruitNameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var fruitDetails: FruitDetails?
    var fruitViewController = FruitViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fruitNameLabel.text = fruitDetails?.type
        if let fruitWeightInGrams = fruitDetails?.weight{
            weightLabel.text = fruitViewController.viewModel.convertWeight(fruitWeight: fruitWeightInGrams)
        }
        if let fruitPriceInPence = fruitDetails?.price {
            priceLabel.text = fruitViewController.viewModel.convertPrice(fruitPrice: fruitPriceInPence)
        }
    }
}
