//
//  FruitViewController.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import UIKit

class FruitViewController: UIViewController {
    @IBOutlet weak var fruitListTableView: UITableView!
    
    private var listOfFruits: Fruit?
    private var listOfFruitsArray = [FruitDetails]()

    private let fruitWebService = FruitWebService()
    private let fruitAppUsageStatsService = FruitAppUsageStatsService()

    var viewModel = FruitViewModel(fruitWebService: FruitWebService())

    override func viewDidLoad() {
        super.viewDidLoad()
        fruitListTableView.tableFooterView = UIView(frame: .zero)
        loadFruitList()
    }
    
    private func loadFruitList() {
        viewModel.getFruitDataFromService() { (fruit, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                self.fruitAppUsageStatsService.saveStatsToServer(event: "error", data: error.localizedDescription)
                return
            }
            guard let fruit = fruit  else { return }
            self.listOfFruitsArray = fruit
            DispatchQueue.main.async {
                self.fruitListTableView.reloadData()
            }
        }
    }
}

extension FruitViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  listOfFruitsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FruitCell") as! FruitListTableViewCell
        cell.fruitNameLabel?.text =  listOfFruitsArray[indexPath.row].type
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fruitDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "FruitDetailVC") as! FruitDetailViewController
        fruitDetailViewController.fruitDetails = listOfFruitsArray[indexPath.row]
        self.navigationController?.pushViewController(fruitDetailViewController, animated: true)
    }
    
}

