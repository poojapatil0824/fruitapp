//
//  FruitWebService.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class FruitWebService {
    private let fruitAppUsageStatsService = FruitAppUsageStatsService()
}

extension FruitWebService: FruitWebServiceProtocol {

    func fetchFruitDataFromServer(completion: @escaping (_ fruit : [FruitDetails]?, _ error: Error?) -> Void) {
        let url = URL(string: "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/data.json")
        let startDate = Date()

        guard let downloadURL = url else {return}
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("\(error.debugDescription)")
                self.fruitAppUsageStatsService.saveStatsToServer(event: "error", data: error.debugDescription)
                return
            }
            do {
                let decoder = JSONDecoder()
                let fruitDataSet = try decoder.decode(Fruit.self, from: data)
                self.sendLoadStatsData(startDate: startDate)
                return completion(fruitDataSet.fruit, nil)

            }catch {
                print("Something went wrong", error)
                self.fruitAppUsageStatsService.saveStatsToServer(event: "error", data: error.localizedDescription)

            }
            }.resume()
    }
    private func sendLoadStatsData(startDate:Date) {
        let executionTime = Date().timeIntervalSince(startDate)
        let dataForStats = String(Int(executionTime * 1000))
        self.fruitAppUsageStatsService.saveStatsToServer(event: "load", data: dataForStats)
    }
}
