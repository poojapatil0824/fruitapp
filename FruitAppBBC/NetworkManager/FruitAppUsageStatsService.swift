//
//  FruitAppUsageStatsService.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class FruitAppUsageStatsService {
    func saveStatsToServer(event: String, data: String) -> Void {
        var items = [URLQueryItem]()
        items.append(URLQueryItem(name: "event", value: event))
        items.append(URLQueryItem(name: "data", value: data))
        var baseURL = URLComponents(string: "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats?")!
        baseURL.queryItems = items
        var urlRequest = URLRequest(url: baseURL.url!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }
        task.resume()
    }
}
