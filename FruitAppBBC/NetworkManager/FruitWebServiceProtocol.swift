//
//  FruitWebServiceProtocol.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 05/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

protocol FruitWebServiceProtocol {
    func fetchFruitDataFromServer(completion: @escaping (_ fruit : [FruitDetails]?, _ error: Error?) -> Void)
}
