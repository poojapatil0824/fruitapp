//
//  FruitListTableViewCell.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class FruitListTableViewCell: UITableViewCell {
    @IBOutlet weak var fruitNameLabel: UILabel!
}
