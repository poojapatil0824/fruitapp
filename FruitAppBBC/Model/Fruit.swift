//
//  Fruit.swift
//  FruitAppBBC
//
//  Created by Pooja Awati on 04/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

struct Fruit: Codable {
    let fruit : [FruitDetails]
}

struct FruitDetails {
    let type: String?
    let price: Int?
    let weight: Int?
}

extension FruitDetails: Codable {
    
    enum CodingKeys: String, CodingKey {
        case type
        case price
        case weight
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        price = try values.decodeIfPresent(Int.self, forKey: .price)
        weight = try values.decodeIfPresent(Int.self, forKey: .weight)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(price, forKey: .price)
        try container.encodeIfPresent(weight, forKey: .weight)
    }
}
