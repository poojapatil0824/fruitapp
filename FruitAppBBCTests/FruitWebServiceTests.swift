//
//  FruitWebServiceTests.swift
//  FruitAppBBCTests
//
//  Created by Pooja Awati on 05/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import XCTest
@testable import FruitAppBBC

class FruitWebServiceTests: XCTestCase {

    let mockFruitWebService = MockFruitWebService()

    func testFruitWebServiceResponse() {
        let expectation = self.expectation(description: "Fruit Web Service Parse Expected")
        mockFruitWebService.fetchFruitDataFromServer(){ json, error in

            XCTAssertNil(error)
            guard json != nil else {
                XCTFail()
                return
            }
            do {
                XCTAssertNotNil(json)
                expectation.fulfill()
            }
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }

}
