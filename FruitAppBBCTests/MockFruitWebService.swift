//
//  MockFruitWebService.swift
//  FruitAppBBCTests
//
//  Created by Pooja Awati on 05/09/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import XCTest
@testable import FruitAppBBC

class  MockFruitWebService {
    var shouldServiceReturnError = false
    var fruitServiceRequestWasCalled = false
    var mockJSONData: Fruit?

    enum MockFruitServiceError: Error{
        case fruit
    }
    func reset(){
        shouldServiceReturnError = false
        fruitServiceRequestWasCalled = false
    }
    convenience init(){
        self.init(false)
    }
    init(_ shouldServiceReturnError: Bool) {
        self.shouldServiceReturnError = shouldServiceReturnError
        self.getMockJSONData()
    }

    func getMockJSONData(){
        if let path = Bundle.main.path(forResource: "data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: [])
                let decoder = JSONDecoder()
                self.mockJSONData = try decoder.decode(Fruit.self, from: data)
            } catch {
                print("Error loading JSON file")
            }
        } else {
            print("File not found")
        }
    }
}
extension MockFruitWebService: FruitWebServiceProtocol {
    func fetchFruitDataFromServer(completion: @escaping ([FruitDetails]?, Error?) -> Void) {
        fruitServiceRequestWasCalled = true

        if shouldServiceReturnError {
            completion(nil, MockFruitServiceError.fruit)
        } else {
            completion(self.mockJSONData?.fruit, nil)
        }
    }
}
