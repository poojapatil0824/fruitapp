##  Fruit App using MVVM design pattern


-   I have used **Swift** and **Xcode 10.3** to develop this app
-   This is **Universal** app and deployment target is supported for **iOS 12**
-   I have implemented **MVVM** design pattern for the project architecture 
-   The Fruit app has two screens - List and Details
-   Regarding app usage stats - I was not sure how to implement that, however I have tried to implement it for **Load** and **Error** events
-	Unit test for Webservice has been added
-	UI test for table view, display and tapping have been added
